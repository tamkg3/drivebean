package vn.citynow.drivebean;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DrivebeanApplication {

    public static void main(String[] args) {
        SpringApplication.run(DrivebeanApplication.class, args);
    }

}
