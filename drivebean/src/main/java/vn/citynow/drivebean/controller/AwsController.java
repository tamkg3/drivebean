package vn.citynow.drivebean.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import vn.citynow.drivebean.service.AmazonS3ClientService;
import vn.citynow.drivebean.service.DataService;

import javax.servlet.http.HttpServletRequest;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/files")
public class AwsController {
    @Autowired
    private AmazonS3ClientService amazonS3ClientService;
    @Autowired
    private DataService dataService;

    @PostMapping
    public Map<String, String> uploadFile(@RequestPart(value = "file") MultipartFile file)
    {
        this.amazonS3ClientService.uploadFileToS3Bucket(file, true);

        Map<String, String> response = new HashMap<>();
        response.put("message", "file [" + file.getOriginalFilename() + "] uploading request submitted successfully.");
        return response;
    }

    @PostMapping("/upload")
    public Map<String, String> uploadFileToFolder(@RequestPart(value = "file") MultipartFile file,
                                                  @RequestPart String filename)
    {
        this.amazonS3ClientService.uploadFileToFolderToS3Bucket(file,filename,true);

        Map<String, String> response = new HashMap<>();
        response.put("message", "file [" + file.getOriginalFilename() + "] uploading request submitted successfully.");

        return response;
    }


    @DeleteMapping
    public Map<String, String> deleteFile(@RequestParam("file_name") String fileName)
    {
        this.amazonS3ClientService.deleteFileFromS3Bucket(fileName);

        Map<String, String> response = new HashMap<>();
        response.put("message", "file [" + fileName + "] removing request submitted successfully.");

        return response;
    }

    @GetMapping("/download")
    public ResponseEntity<Resource> downloadFile(@RequestParam String fileName, HttpServletRequest request){
        // Load file as Resource
        Resource resource = null;
        String contentType = null;
        try {

            resource = this.amazonS3ClientService.loadFileAsResource(fileName);


            // Try to determine file's content type
            int ext = resource.getFilename().lastIndexOf(".");
            contentType = request.getServletContext().getMimeType(resource.getFilename().substring(ext));
        }catch (FileNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,"resource mistake",e);
        }

        // Fallback to the default content type if type could not be determined
        if(contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION,
                        "attachement; filename=\""+ resource.getFilename()+"\"")
                .body(resource);
    }
    @PostMapping("/createFolder")
    public void createFolder(@RequestParam String fullPath){
        this.amazonS3ClientService.createFolder(fullPath);

    }

}
