package vn.citynow.drivebean.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;


@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class DataDTO {
    private Long id;

    private String name;

    private String keyPath;

    private Date dateCreate;

    private Date lastUpdate;

    private boolean starLabel;

    private String description;

    private Long userId;
}
