package vn.citynow.drivebean.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class UserDTO {

    private Long id;

    private String email;


    private String fullName;


    private String password;


    private String avatar;


    private Date dateCreated;
    //private List<DataDTO> dataOfUser;

    @Override
    public String toString(){
        return id +"-"+email+"-"+fullName;
    }
}
