package vn.citynow.drivebean.exception;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.multipart.MaxUploadSizeExceededException;

import java.io.IOException;

@ControllerAdvice
public class FileHandleException {
    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handle(Exception ex) {
        if (ex instanceof IllegalArgumentException) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("no illegal access to object");
        }else if(ex instanceof MaxUploadSizeExceededException) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("File Max Upload file exceeded");
        }if (ex instanceof IOException){
            return ResponseEntity.badRequest().body(ex);
        }
        return ResponseEntity .status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }
}
