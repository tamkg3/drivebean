package vn.citynow.drivebean.mapper;


import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import vn.citynow.drivebean.dto.DataDTO;
import vn.citynow.drivebean.model.Data;

import java.util.List;


@Mapper(componentModel = "spring")
public interface DataMapper {
    @Mapping(target = "user.id",source = "userId")
    Data toData(DataDTO dataDTO);
    List<Data> toDataList(List<DataDTO> dataDTOS);

    @Mapping(target = "userId",source = "user.id")
    DataDTO toDataDTO(Data entity);
    List<DataDTO> toDataDTOList(List<Data> entity);


}
