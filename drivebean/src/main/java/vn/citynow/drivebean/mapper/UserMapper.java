package vn.citynow.drivebean.mapper;


import org.mapstruct.Mapper;
import vn.citynow.drivebean.dto.UserDTO;
import vn.citynow.drivebean.model.User;

import java.util.List;


@Mapper(componentModel = "spring")
public interface UserMapper {


    User toUser(UserDTO userDTO);
    List<User> toUserList(List <UserDTO> UserDTOs);


    UserDTO toUserDTO(User user);
    List<UserDTO> toUserDTOList(List <User> entity);
}
