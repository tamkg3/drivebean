package vn.citynow.drivebean.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EntityListeners(AuditingEntityListener.class)
public class Data implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 50, nullable = false)
    private String name;

    @CreatedDate
    private Date dateCreated;

    @LastModifiedDate
    private Date lastUpdate;

    private String decription;

    private boolean starLabel;

    @Column(unique = true)
    private String keyPath;


    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_user_owner", nullable = false)
    private User user;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_type", referencedColumnName = "id", nullable = false)
    private Type type;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_license", nullable = false)
    private License license;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "user_access", joinColumns = @JoinColumn(name = "id_data", referencedColumnName = "id")
            , inverseJoinColumns = @JoinColumn(name = "id_user", referencedColumnName = "id"))
    private Set<User> users;



}
