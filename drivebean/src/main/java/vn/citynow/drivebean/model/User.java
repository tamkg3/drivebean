package vn.citynow.drivebean.model;

import lombok.*;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@EntityListeners(AuditingEntityListener.class)
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true, length = 50, nullable = false)
    private String email;

    @Column(length = 100, nullable = false)
    private String fullName;


    @Column(length = 100, nullable = false)
    private String password;

    @Column(length = 100)
    private String avatar;

    @CreatedDate
    private Date dateCreated;



    @ManyToMany(mappedBy = "user")
    private Set<Data> dataAccess = new HashSet<>();

    @OneToMany(mappedBy = "user")
    private List<Data> dataOfUser;




    @Override
    public String toString(){
        return id +"-"+email+"-"+fullName;
    }
}
