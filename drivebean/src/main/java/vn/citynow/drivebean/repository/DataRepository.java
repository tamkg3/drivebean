package vn.citynow.drivebean.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vn.citynow.drivebean.model.Data;

@Repository
public interface DataRepository extends JpaRepository<Data,Long> {
}
