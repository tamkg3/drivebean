package vn.citynow.drivebean.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vn.citynow.drivebean.model.User;

@Repository
public interface UserRepository extends JpaRepository<User,Long> {

}
