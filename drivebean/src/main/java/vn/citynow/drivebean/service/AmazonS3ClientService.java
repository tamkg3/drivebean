package vn.citynow.drivebean.service;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileNotFoundException;

public interface AmazonS3ClientService {
    void uploadFileToS3Bucket(MultipartFile multipartFile, boolean enablePublicReadAccess);

    //filename is a key to download/upload, it includes path + f
    void deleteFileFromS3Bucket(String fileName);

    //S3Object downloadFileFromS3Bucket(String fileName);

    Resource loadFileAsResource(String filename) throws FileNotFoundException;

    void uploadFileToFolderToS3Bucket(MultipartFile file, String fileName,boolean enablePublicReadAccess);

    boolean existFile(String fileName);

    void createFolder(String fullFolderPath);
}
