package vn.citynow.drivebean.service;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URI;


// add public to anyone can download: putObjectRequest.withCannedAcl(CannedAccessControlList.PublicRead);

@Component
public class AmazonS3ClientServiceImpl implements AmazonS3ClientService
{
    private String awsS3AudioBucket;
    private AmazonS3 amazonS3;
    private static final Logger logger = LoggerFactory.getLogger(AmazonS3ClientServiceImpl.class);

    @Autowired
    public AmazonS3ClientServiceImpl(Region awsRegion, AWSCredentialsProvider awsCredentialsProvider, String awsS3AudioBucket)
    {
        this.amazonS3 = AmazonS3ClientBuilder.standard()
                .withCredentials(awsCredentialsProvider)
                .withRegion(awsRegion.getName()).build();
        this.awsS3AudioBucket = awsS3AudioBucket;

    }

    @Async
    public void uploadFileToS3Bucket(MultipartFile multipartFile, boolean enablePublicReadAccess)
    {
        String fileName = multipartFile.getOriginalFilename();

        try {
            //creating the file in the server (temporarily)
            File file = new File(fileName);
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(multipartFile.getBytes());
            fos.close();

            PutObjectRequest putObjectRequest = new PutObjectRequest(this.awsS3AudioBucket, fileName, file);

            if (enablePublicReadAccess) {
                putObjectRequest.withCannedAcl(CannedAccessControlList.PublicRead);
            }
            this.amazonS3.putObject(putObjectRequest);
            //removing the file created in the server
            file.delete();
        } catch (IOException | AmazonServiceException ex) {
            logger.error("error [" + ex.getMessage() + "] occurred while uploading [" + fileName + "] ");
        }

    }

    @Async
    public void deleteFileFromS3Bucket(String fileName)
    {
        try {
            amazonS3.deleteObject(new DeleteObjectRequest(awsS3AudioBucket, fileName));
        } catch (AmazonServiceException ex) {
            logger.error("error [" + ex.getMessage() + "] occurred while removing [" + fileName + "] ");
        }
    }

    /*
    @Override
    public S3Object downloadFileFromS3Bucket(String fileName) {
        S3Object object = amazonS3.getObject(awsS3AudioBucket,fileName);
        return object;
    }

     */
    @Override
    public Resource loadFileAsResource(String fileName) throws FileNotFoundException {
        try {

            S3Object s3Object = amazonS3.getObject(awsS3AudioBucket,fileName);
            URI urlResource = s3Object.getObjectContent().getHttpRequest().getURI();
            Resource resource = new UrlResource(urlResource);

            if (resource.exists()) {
                return resource;
            } else {
                throw new FileNotFoundException("File not found " + fileName);
            }
        }catch (MalformedURLException ex) {
            throw new FileNotFoundException("File not found " + fileName +ex);
        }
    }

    @Override
    public void uploadFileToFolderToS3Bucket(MultipartFile multipartFile, String path,boolean enablePublicReadAccess) {

        String fileName = multipartFile.getOriginalFilename();
        try {
            //creating the file in the server (temporarily)
            File file = new File(fileName);

            FileOutputStream fos = new FileOutputStream(file);
            fos.write(multipartFile.getBytes());
            fos.close();

            PutObjectRequest putObjectRequest = new PutObjectRequest(this.awsS3AudioBucket, path+"/"+fileName, file);

            if (enablePublicReadAccess) {
                putObjectRequest.withCannedAcl(CannedAccessControlList.PublicRead);
            }
            this.amazonS3.putObject(putObjectRequest);
            //removing the file created in the server
            file.delete();
        } catch (IOException | AmazonServiceException ex) {
            logger.error("error [" + ex.getMessage() + "] occurred while uploading [" + fileName + "] ");
        }
    }

    @Override
    public boolean existFile(String fileName) {
        if(amazonS3.getObject(awsS3AudioBucket,fileName) == null)
        {
            return false;
        }
        return true;

    }

    @Override
    public void createFolder(String fullFolderPath) {
        ObjectMetadata metadata = new ObjectMetadata();
        metadata.setContentLength(0);
        try {
            InputStream emptyContent = new ByteArrayInputStream(new byte[0]);

            // create a PutObjectRequest passing the folder name suffixed by /
            PutObjectRequest putObjectRequest = new PutObjectRequest(this.awsS3AudioBucket,
                    fullFolderPath + "/", emptyContent, metadata);

            // send request to S3 to create folder
            this.amazonS3.putObject(putObjectRequest);
        }catch (AmazonServiceException ex) {

            logger.error("error [" + ex.getMessage() + "] occurred while create Folder [" + fullFolderPath.substring(
                    fullFolderPath.lastIndexOf("/")) + "] ");
        }


    }

}