package vn.citynow.drivebean.service;

import vn.citynow.drivebean.dto.DataDTO;

public interface DataService{
     void save(DataDTO dataDTO);
    DataDTO getById(Long id);

}

