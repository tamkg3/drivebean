package vn.citynow.drivebean.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.citynow.drivebean.dto.DataDTO;
import vn.citynow.drivebean.mapper.DataMapper;
import vn.citynow.drivebean.repository.DataRepository;

@Service
public class DataServiceImpl implements DataService {


    private final DataRepository dataRepository;
    private final DataMapper dataMapper;

    @Autowired
    public DataServiceImpl(DataRepository dataRepository, DataMapper dataMapper) {
        this.dataRepository = dataRepository;
        this.dataMapper = dataMapper;
    }


    @Override
    public void save(DataDTO dataDTO) {
        dataRepository.save(dataMapper.toData(dataDTO));
    }

    @Override
    public DataDTO getById(Long id) {
        try
        {
            return dataMapper.toDataDTO(dataRepository.getOne(id));
        }catch (IllegalArgumentException ex){
            throw new IllegalArgumentException(ex);
        }
    }


}
